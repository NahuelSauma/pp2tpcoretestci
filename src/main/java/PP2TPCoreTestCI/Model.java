package PP2TPCoreTestCI;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Model {
	List<Long> votes;
	List<String> parties;
	List<Observer> registry;
	
	public Model (List<String> partyNames,List<Long> partyVotes){
		parties = partyNames;
		votes = partyVotes;
		registry = new ArrayList<Observer>();
	};
	
	//interface de acceso para modificacion por controlador
	public void clearVotes() {
		for(Long voto: votes) {
			voto = 0L;
		}
		notificar();
	};
	
	
	public void metodoDePrueba() {
		//asd
		//asd
		//asd
	}
	
	public void changeVote () {
		for (int i=0; i<votes.size(); i++) {
			Long sum = votes.get(i)+1L;
			votes.set(i, sum);
		}
		notificar();
	};
	
	//funciones de acceso a los datos
	public Iterator <Long> makeVoteIterator(){
		Iterator<Long> iterator = votes.iterator();
		return iterator;
	}
	public Iterator <String> makePartyIterator(){
		Iterator<String> iterator = parties.iterator();
		return iterator;
	}
	
	public List<Long> getVotes() {
		return this.votes;
	}
	public List<String> getParties(){
		return this.parties;
	}
	
	
	public void attach(Observer s) {
		registry.add(s);
	}
	public void detach(Observer s) {
		registry.remove(s);
	}
	
	public void notificar() {
		for(Observer ob : registry) {
			ob.update();
		}
	};
}
